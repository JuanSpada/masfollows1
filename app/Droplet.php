<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Account;

class Droplet extends Model
{
    protected $fillable = [
        'name'
    ];

    public function usage()
    {
        $accounts = $this->accounts;
        return $accounts->count();
    }

    public function accounts()
    {
        return $this->hasMany(Account::class);
    }
}