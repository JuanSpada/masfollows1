<?php

namespace App;

use App\hashtags;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    public function hashtags()
    {
        return $this->hasMany(Hashtag::class);
    }
}
