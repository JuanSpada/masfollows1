<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Services\DropletService;

class DropletsStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'droplet-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if you need to create a new droplet';

    private $dropletService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DropletService $dropletService)
    {
        parent::__construct();
        $this->dropletService = $dropletService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->dropletService->dropletsRoutine();
    }
}
