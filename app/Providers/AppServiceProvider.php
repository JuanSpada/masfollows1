<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Http\Clients\ZapierClient;
use App\Http\Clients\DigitalOceanClient;
use App\Http\Services\DropletService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(ZapierClient::class);
        $this->app->when(DropletService::class);
        $this->app->when(DigitalOceanClient::class);
    }

    

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}