<?php

namespace App\Http\Services;

use DB;
use App\Droplet;
use App\Account;
use Illuminate\Http\Request;
use App\Http\Clients\DigitalOceanClient;

class DropletService 
{

    private $digitalOceanClient;

    public function __construct(DigitalOceanClient $digitalOceanClient)
    {
        $this->digitalOceanClient = $digitalOceanClient;    
    }

    public function processAccount(Request $request)
    {
        $droplet = $this->getUsableDroplet();
        if (!$droplet) {
            event($this->dropletsRoutine());
            return route('backoffice.account.index');
        }
        
        $account = new Account();
        $account->username = $request->username;
        $account->password = $request->password;
        $account->status = 0;
        $account->droplet_id = $droplet->id;
        $account->user_id = $request->user()->id;
        $account->save();

        // $this->instagramAuthenticate($droplet);
        
        return route('backoffice.account.index');
    }

    public function dropletsRoutine()
    {
        $droplets = $this->getAvailableDroplets();
        if ($droplets->count() >= 2) {
            return;
        }
        $this->createNewDroplet();
    }

    public function getAvailableDroplets()
    {
        $droplets = Droplet::where(DB::raw('(SELECT COUNT(*) FROM `accounts` WHERE `accounts`.droplet_id = `droplets`.id)'), '<' , "3")->get();
        return $droplets;
    }

    public function getUsableDroplet()
    {
        $droplets = $this->getAvailableDroplets();
        if (!$droplets->isEmpty()) {
            return $droplets->first();
        }
        return null;
    }

    public function createNewDroplet()
    {
        $name = uniqid()."-MasFollows";
        $resonse = $this->digitalOceanClient->createDroplet($name);
        if ($resonse && $resonse->getStatusCode() == 202) {
            $droplet = new Droplet();
            $droplet->name = $name;
            $droplet->ip = '';
            $droplet->droplet_id = 2;
            $droplet->save();
        }
    }

    public function instagramAuthenticate(Droplet $droplet)
    {

        dd('pk');   
    }
}