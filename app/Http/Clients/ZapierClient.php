<?php

namespace App\Http\Clients;


class ZapierClient
{
    public function registerOnHubSpot(string $name, string $email) : void
    {
        if (!$this->isEnabled()) {
            return;
        }

        $data = [
            'nombre' =>  $name,
            'email' => $email,
        ];
        $response = $this->request(config('zapier.url'), "POST", $data);
    }

    private function isEnabled() : bool
    {
        return config('zapier.enabled');
    }
    
    private function request($url, $method, $data = [])
    {
        $client = new \GuzzleHttp\Client();

        $parms = [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            'form_params' => $data,
        ];
        $res = $client->request($method, $url, $parms);

        return $res->getBody()->getContents();
    }
}
