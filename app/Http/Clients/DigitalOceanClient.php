<?php

namespace App\Http\Clients;


class DigitalOceanClient
{

    public function createDroplet(string $name)
    {
        if (!$this->isEnabled()) {
            return;
        }

        $data = [
            "name" => $name,
            "region" => "nyc1",
            "size" => "s-1vcpu-1gb",
            "image" => 50996261,
            "ssh_keys" => [
                "fe:22:2f:d5:54:8d:f0:95:79:ce:99:1a:43:39:32:b3"
            ],
            "backups" => false,
            "ipv6" => true,
            "user_data" => null,
            "private_networking" => null,
            "volumes" => null,
            "tags" => [
                "web"
            ],
        ];

        return $this->request(\config('digitalocean.url')."droplets", "POST", $data);
    }

    private function isEnabled() : bool
    {
        return \config('digitalocean.enabled');
    }
    
    private function request($url, $method, $data = [])
    {
        $client = new \GuzzleHttp\Client();
        
        $parms = [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer '.\config('digitalocean.token') 
            ],
            'json' => $data,
        ];
        $res = $client->request($method, $url, $parms);

        return $res;
    }
}
