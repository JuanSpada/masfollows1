@extends('layouts.back')
@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Usuario: {{$user->name}} | Email: {{$user->email}} || Fecha: {{$user->created_at->diffForHumans()}}</h6>
    </div>
<div>
<div class="row">
    <div class="col m-3">
        <form action="{{route ('editEstado', $user->id)}}" method="POST">
            @csrf
            <div class="row">
                <div class="col form-group col-md-6">
                    <label for="estado">Estado: {{$user->showEstado()}}</label>
                    <select class="form-control" name="estado" id="estado">
                        <option disabled selected>Cambiar Estado</option>
                        <option value="0">Inactivo</option>
                        <option value="1">Activo</option>
                        <option value="3">En Revisión</option>
                        <option value="4">Error en la Validación</option>
                    </select>
                    <hr>
                    <label for="">Usuario</label>
                    <input class="form-control" type="text" name="cuenta" id="cuenta" value="{{$user->cuenta}}">
                    <br>
                    <label for="">Contraseña</label>
                    <input class="form-control" type="text" disabled value="{{$user->igpassword}}">
                </div>
                <div class="col form-group col-md-6">
                    <label for="assigned_to">Assigned to: {{$user->showAssignedTo()}}</label>
                    <select class="form-control" name="assigned_to" id="assigned_to">
                        <option disabled selected>Asignar a</option>
                        <option value="1">Juan</option>
                        <option value="2">Pao</option>
                        <option value="3">196.17.105.210:53098</option>
                    </select>
                    <hr>
                    @if($user->cliente)
                        <div>
                            <a href="{{route ('baja', $user->id)}}">Baja Cliente</a>
                        </div>
                    @endif
                    <a href="{{route ('userDelete', $user->id)}}">Eliminar Usuario</a>
                    <h5>Customizaciones:</h5>

                    @if(Auth::user()->customizations()[0]->follow)
                        <p><i class="far fa-check-square mr-2" style="color: #f15f79;"></i>Seguir y Dejar de Seguir</p>
                    @else
                        <p><i class="far fa-square mr-2" style="color: #f15f79;"></i>Seguir y Dejar de Seguir</p>
                    @endif

                    @if(Auth::user()->customizations()[0]->likes)
                        <p><i class="far fa-check-square mr-2" style="color: #f15f79;"></i>Likes</p>
                    @else
                        <p><i class="far fa-square mr-2" style="color: #f15f79;"></i>Likes</p>
                    @endif

                    @if(Auth::user()->customizations()[0]->comment)
                        <p><i class="far fa-check-square mr-2" style="color: #f15f79;"></i>Comentarios</p>
                    @else
                        <p><i class="far fa-square mr-2" style="color: #f15f79;"></i>Comentarios</p>
                    @endif
                    @if(Auth::user()->customizations()[0]->private)
                    <p><i class="far fa-check-square mr-2" style="color: #f15f79;"></i>Ignorar Usuarios Privados</p>
                    @else
                    <p><i class="far fa-square mr-2" style="color: #f15f79;"></i>Ignorar Usuarios Privados</p>
                    @endif
                </div>
            </div>
            <hr>
            <label for="">Hashtags:</label>
            <div class="row">
                <div class="col form-group">
                    <input class="form-control" type="text" name="hashtag1" id="hashtag1" value="{{$user->hashtag1}}">
                    <input class="form-control" type="text" name="hashtag3" id="hashtag3" value="{{$user->hashtag3}}">
                </div>
                <div class="col form-group">
                        <input class="form-control" type="text" name="hashtag2" id="hashtag2" value="{{$user->hashtag2}}">
                    <input class="form-control" type="text" name="hashtag4" id="hashtag4" value="{{$user->hashtag4}}">
                </div>
            </div>
            <hr>
            <label for="">Referencias:</label>
            <div class="row">
                <div class="col form-group">
                    <input class="form-control" type="text" name="referencia1" id="referencia1" value="{{$user->referencia1}}">
                    <input class="form-control" type="text" name="referencia3" id="referencia3" value="{{$user->referencia3}}">
                </div>
                <div class="col form-group">
                    <input class="form-control" type="text" name="referencia2" id="referencia2" value="{{$user->referencia2}}">
                    <input class="form-control" type="text" name="referencia4" id="referencia4" value="{{$user->referencia4}}">
                </div>
            </div>
            <div class="d-flex justify-content-center">
                <button class="btn btn-primary col-md-6" type="submit">Editar</button>
            </div>
        </form>
        <hr>
        <p>Si esta mal la contraseña o usuario de instagram poner como estado Error en la Validación.</p>
        </div>
    <div class="col m-3">
        <p><strong>1 Paso:</strong> <a href="/defaultprueba.py" download="">Descargar Script Usurio de Prueba</a> o <a href="#">Descargar Script Cliente</a>.</p>
        <p><strong>2 Paso:</strong> Cambiar de  nombre del script por "{{$user->cuenta}}.py".</p>
        <p><strong>3 Paso:</strong> Agregar el Script a la carpeta InstaPy.</p>
        <p><strong>4 Paso:</strong> Editar el script con los datos del cliente.</p>
        <hr>
        <p><strong>5 Paso correr el Script y poner como activo al usuario:</strong></p>
        <label><strong>Console:</strong></label>
        <input class="form-control" type="text" disabled value="python {{$user->cuenta}}.py">
        <hr>
        <p>Solo correr script en las cuentas que tenes asignadas. <strong>POR QUE SI CORREMOS 2 CUENTAS EN DIFERENTES LUGARES O 2 CUENTAS A LA VES LES PUEDEN SUSPENDER LA CUENTA</strong></p>
        <p>Por las dudas no correr + 10 scripts por días en mismo IP, tampoco pasarse de las 700 acciones diarias ni las 100 acciones por dia.</p>
        <p>A laburar perro</p>
    </div>
</div>

@endsection
