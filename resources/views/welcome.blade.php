<!DOCTYPE html>
<html lang="es">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-T8VLC9M');</script>
    <!-- End Google Tag Manager -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Sistema de crecimiento para Instagram, crecimiento real, seguidores reales.">
    <link rel="icon" type="png" href="/img/favicon.png">
    
    <title>MásFollows</title>
    
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    
    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    
    <!-- Custom styles for this template -->
    <link href="{{ asset('css/landing-page.css') }}" rel="stylesheet">
    
    <!-- Font Awesome -->
    <script src="https://kit.fontawesome.com/b59db35431.js"></script>
    
    
</head>

<body>
    
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T8VLC9M"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        
        <!-- Navigation -->
        
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            {{-- <div class="sidebar-brand-icon rotate-n-15">
                <i class="fas fa-laugh-wink"></i>
            </div> --}}
            <img src="/img/logo.png" style="width: 35px;" alt="">
            <a class="navbar-brand ml-3" style="color: #f15f79;" href="#">MásFollows</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" style="color: #f15f79;" href="{{ route('login') }}">{{ trans('welcome.nav.login') }}</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" style="color: #f15f79;" href="{{ route('register') }}">{{ trans('welcome.nav.register') }}</a>
                    </li>
                </ul>
            </div>
        </nav>
        
        <!-- Masthead -->
        <header class="masthead text-white text-center">
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-xl-9 mx-auto">
                        <h1 class="mb-2">{{ trans('welcome.slider.title') }}</h1>
                        <p>{{ trans('welcome.slider.subtitle') }}</p>
                        <button class="btn btn-danger"> <a style="color:white;" href="#showcase">{{ trans('buttons.moreinfo') }}</a></button>
                    </div>
                </div>
            </div>
        </header>
        
        <!-- Icons Grid -->
        <section class="features-icons bg-light text-center">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                            <div class="features-icons-icon d-flex">
                                <i class="fas fa-search m-auto"></i>
                            </div>
                            <h3>{{ trans('welcome.section1.title1') }}</h3>
                            <p class="lead mb-0">{{ trans('welcome.section1.desc1') }}</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                            <div class="features-icons-icon d-flex">
                                <i class="fas fa-users m-auto"></i>
                            </div>
                            <h3>{{ trans('welcome.section1.title2') }}</h3>
                            <p class="lead mb-0">{{ trans('welcome.section1.desc2') }}</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="features-icons-item mx-auto mb-0 mb-lg-3">
                            <div class="features-icons-icon d-flex">
                                <i class="fas fa-check m-auto"></i>
                            </div>
                            <h3>{{ trans('welcome.section1.title3') }}</h3>
                            <p class="lead mb-0">{{ trans('welcome.section1.desc3') }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <!-- Image Showcases -->
        <section id="showcase" class="showcase">
            <div class="container-fluid p-0">
                <div class="row no-gutters">
                    <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image: url('img/img1.jpg');"></div>
                    <div class="col-lg-6 order-lg-1 my-auto showcase-text">
                        <h2>{{ trans('welcome.section2.title1') }}</h2>
                        <p class="lead mb-0">{{ trans('welcome.section2.desc1') }}</p>
                    </div>
                </div>
                <div class="row no-gutters">
                    <div class="col-lg-6 text-white showcase-img" style="background-image: url('img/img2.jpg');"></div>
                    <div class="col-lg-6 my-auto showcase-text">
                        <h2>{{ trans('welcome.section2.title2') }}</h2>
                        <p class="lead mb-0">{{ trans('welcome.section2.desc2') }}</p>
                    </div>
                </div>
                <div class="row no-gutters">
                    <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image: url('img/img3.jpg');"></div>
                    <div class="col-lg-6 order-lg-1 my-auto showcase-text">
                        <h2>{{ trans('welcome.section2.title3') }}</h2>
                        <p class="lead mb-0">{{ trans('welcome.section2.desc3') }}</p>
                    </div>
                </div>
            </div>
        </section>
        
        <!-- Call to Action -->
        <section id="register" class="call-to-action text-white text-center">
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-xl-9 mx-auto">
                        <h2 class="mb-4 text-white">{{ trans('welcome.contact.title') }}</h2>
                    </div>
                    <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
                        <form method="POST" action="{{ route('register') }}" accept="application/json" id="register">
                            @csrf
                            <div class="form-row">
                                <div class="col-6 mb-2">
                                    <input class="form-control @error('name') is-invalid @enderror" type="text" name="name" id="name" placeholder="{{ trans('welcome.contact.name') }}" required value="{{ old('name') }}">
                                    @error('name')
                                    <span class="invalid-feedback text-white" role="alert">
                                        <strong style="font-size:1.2em;">{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-6 mb-2">
                                    <select class="form-control" name="pais" id="pais">
                                        <option value="" disabled selected required>{{ trans('welcome.contact.country') }}</option>
                                        <option value="AF">
                                            Afganistán
                                        </option>
                                        <option value="AX">
                                            Islas Aland
                                        </option>
                                        <option value="AL">
                                            Albania
                                        </option>
                                        <option value="DZ">
                                            Argelia
                                        </option>
                                        <option value="AS">
                                            Samoa Americana
                                        </option>
                                        <option value="AD">
                                            Andorra
                                        </option>
                                        <option value="AO">
                                            Angola
                                        </option>
                                        <option value="AI">
                                            Anguila
                                        </option>
                                        <option value="AQ">
                                            Antártida
                                        </option>
                                        <option value="AG">
                                            Antigua y Barbuda
                                        </option>
                                        <option value="AR">
                                            Argentina
                                        </option>
                                        <option value="AM">
                                            Armenia
                                        </option>
                                        <option value="AW">
                                            Aruba
                                        </option>
                                        <option value="AU">
                                            Australia
                                        </option>
                                        <option value="AT">
                                            Austria
                                        </option>
                                        <option value="AZ">
                                            Azerbaiyán
                                        </option>
                                        <option value="BS">
                                            Bahamas
                                        </option>
                                        <option value="BH">
                                            Bahrein
                                        </option>
                                        <option value="BD">
                                            Bangladesh
                                        </option>
                                        <option value="BB">
                                            Barbados
                                        </option>
                                        <option value="BY">
                                            Bielorrusia
                                        </option>
                                        <option value="BE">
                                            Bélgica
                                        </option>
                                        <option value="BZ">
                                            Belice
                                        </option>
                                        <option value="BJ">
                                            Benin
                                        </option>
                                        <option value="BM">
                                            islas Bermudas
                                        </option>
                                        <option value="BT">
                                            Bután
                                        </option>
                                        <option value="BO">
                                            Bolivia
                                        </option>
                                        <option value="BA">
                                            Bosnia y Herzegovina
                                        </option>
                                        <option value="BW">
                                            Botswana
                                        </option>
                                        <option value="BV">
                                            Isla Bouvet
                                        </option>
                                        <option value="BR">
                                            Brasil
                                        </option>
                                        <option value="IO">
                                            Territorio Británico del Océano Índico
                                        </option>
                                        <option value="VG">
                                            Islas Vírgenes Británicas
                                        </option>
                                        <option value="BN">
                                            Brunei Darussalam
                                        </option>
                                        <option value="BG">
                                            Bulgaria
                                        </option>
                                        <option value="BF">
                                            Burkina Faso
                                        </option>
                                        <option value="BI">
                                            Burundi
                                        </option>
                                        <option value="KH">
                                            Camboya
                                        </option>
                                        <option value="CM">
                                            Camerún
                                        </option>
                                        <option value="CA">
                                            Canadá
                                        </option>
                                        <option value="IC">
                                            Islas Canarias
                                        </option>
                                        <option value="CV">
                                            Cabo Verde
                                        </option>
                                        <option value="KY">
                                            Islas Caimán
                                        </option>
                                        <option value="CF">
                                            República Centroafricana
                                        </option>
                                        <option value="TD">
                                            Chad
                                        </option>
                                        <option value="CL">
                                            Chile
                                        </option>
                                        <option value="CN">
                                            China
                                        </option>
                                        <option value="CX">
                                            Isla de Navidad
                                        </option>
                                        <option value="CC">
                                            Islas Cocos (Keeling)
                                        </option>
                                        <option value="CO">
                                            Colombia
                                        </option>
                                        <option value="KM">
                                            Comoras
                                        </option>
                                        <option value="CG">
                                            Congo
                                        </option>
                                        <option value="CD">
                                            Congo (República Democrática del)
                                        </option>
                                        <option value="CK">
                                            Islas Cook
                                        </option>
                                        <option value="CR">
                                            Costa Rica
                                        </option>
                                        <option value="CI">
                                            Costa de Marfil
                                        </option>
                                        <option value="HR">
                                            Croacia
                                        </option>
                                        <option value="CW">
                                            Curazao
                                        </option>
                                        <option value="CU">
                                            Cuba
                                        </option>
                                        <option value="CY">
                                            Chipre
                                        </option>
                                        <option value="CZ">
                                            República Checa
                                        </option>
                                        <option value="DK">
                                            Dinamarca
                                        </option>
                                        <option value="DJ">
                                            Djibouti
                                        </option>
                                        <option value="DM">
                                            Dominica
                                        </option>
                                        <option value="DO">
                                            República Dominicana
                                        </option>
                                        <option value="EC">
                                            Ecuador
                                        </option>
                                        <option value="EG">
                                            Egipto
                                        </option>
                                        <option value="SV">
                                            El Salvador
                                        </option>
                                        <option value="GQ">
                                            Guinea Ecuatorial
                                        </option>
                                        <option value="ER">
                                            Eritrea
                                        </option>
                                        <option value="EE">
                                            Estonia
                                        </option>
                                        <option value="ET">
                                            Etiopía
                                        </option>
                                        <option value="FK">
                                            Islas Malvinas
                                        </option>
                                        <option value="FO">
                                            Islas Faroe
                                        </option>
                                        <option value="FJ">
                                            Fiyi
                                        </option>
                                        <option value="FI">
                                            Finlandia
                                        </option>
                                        <option value="FR">
                                            Francia
                                        </option>
                                        <option value="GF">
                                            Guayana francés
                                        </option>
                                        <option value="PF">
                                            Polinesia francés
                                        </option>
                                        <option value="TF">
                                            Territorios Franceses del Sur
                                        </option>
                                        <option value="GA">
                                            Gabón
                                        </option>
                                        <option value="GM">
                                            Gambia
                                        </option>
                                        <option value="GE">
                                            Georgia
                                        </option>
                                        <option value="DE">
                                            Alemania
                                        </option>
                                        <option value="GH">
                                            Ghana
                                        </option>
                                        <option value="GI">
                                            Gibraltar
                                        </option>
                                        <option value="GR">
                                            Grecia
                                        </option>
                                        <option value="GL">
                                            Tierra Verde
                                        </option>
                                        <option value="GD">
                                            Granada
                                        </option>
                                        <option value="GP">
                                            Guadalupe
                                        </option>
                                        <option value="GU">
                                            Guam
                                        </option>
                                        <option value="GT">
                                            Guatemala
                                        </option>
                                        <option value="GG">
                                            Guernesey
                                        </option>
                                        <option value="GN">
                                            Guinea
                                        </option>
                                        <option value="GW">
                                            Guinea-Bissau
                                        </option>
                                        <option value="GY">
                                            Guayana
                                        </option>
                                        <option value="HT">
                                            Haití
                                        </option>
                                        <option value="HM">
                                            Islas Heard y Mcdonald
                                        </option>
                                        <option value="HN">
                                            Honduras
                                        </option>
                                        <option value="HK">
                                            Hong Kong
                                        </option>
                                        <option value="HU">
                                            Hungría
                                        </option>
                                        <option value="IS">
                                            Islandia
                                        </option>
                                        <option value="IN">
                                            India
                                        </option>
                                        <option value="ID">
                                            Indonesia
                                        </option>
                                        <option value="IR">
                                            Corrí
                                        </option>
                                        <option value="IQ">
                                            Irak
                                        </option>
                                        <option value="IE">
                                            Irlanda
                                        </option>
                                        <option value="IM">
                                            Isla de Man
                                        </option>
                                        <option value="IL">
                                            Israel
                                        </option>
                                        <option value="IT">
                                            Italia
                                        </option>
                                        <option value="JM">
                                            Jamaica
                                        </option>
                                        <option value="JP">
                                            Japón
                                        </option>
                                        <option value="JE">
                                            Jersey
                                        </option>
                                        <option value="JO">
                                            Jordán
                                        </option>
                                        <option value="KZ">
                                            Kazajstán
                                        </option>
                                        <option value="KE">
                                            Kenia
                                        </option>
                                        <option value="KI">
                                            Kiribati
                                        </option>
                                        <option value="KP">
                                            Corea (República Popular Democrática)
                                        </option>
                                        <option value="KR">
                                            República de Corea)
                                        </option>
                                        <option value="KW">
                                            Kuwait
                                        </option>
                                        <option value="KG">
                                            Kirguistán
                                        </option>
                                        <option value="LA">
                                            Lao (República Democrática Popular)
                                        </option>
                                        <option value="LV">
                                            Letonia
                                        </option>
                                        <option value="LB">
                                            Líbano
                                        </option>
                                        <option value="LS">
                                            Lesoto
                                        </option>
                                        <option value="LR">
                                            Liberia
                                        </option>
                                        <option value="LY">
                                            Libia
                                        </option>
                                        <option value="LI">
                                            Liechtenstein
                                        </option>
                                        <option value="LT">
                                            Lituania
                                        </option>
                                        <option value="LU">
                                            Luxemburgo
                                        </option>
                                        <option value="MO">
                                            Macao
                                        </option>
                                        <option value="MK">
                                            macedonia
                                        </option>
                                        <option value="MG">
                                            Madagascar
                                        </option>
                                        <option value="MW">
                                            Malawi
                                        </option>
                                        <option value="MY">
                                            Malasia
                                        </option>
                                        <option value="MV">
                                            Maldivas
                                        </option>
                                        <option value="ML">
                                            Mali
                                        </option>
                                        <option value="MT">
                                            Malta
                                        </option>
                                        <option value="MH">
                                            Islas Marshall
                                        </option>
                                        <option value="MQ">
                                            Martinica
                                        </option>
                                        <option value="MR">
                                            Mauritania
                                        </option>
                                        <option value="MU">
                                            Mauricio
                                        </option>
                                        <option value="YT">
                                            Mayotte
                                        </option>
                                        <option value="MX">
                                            México
                                        </option>
                                        <option value="FM">
                                            Micronesia
                                        </option>
                                        <option value="MD">
                                            Moldavia
                                        </option>
                                        <option value="MC">
                                            Mónaco
                                        </option>
                                        <option value="MN">
                                            Mongolia
                                        </option>
                                        <option value="ME">
                                            Montenegro
                                        </option>
                                        <option value="MS">
                                            Montserrat
                                        </option>
                                        <option value="MA">
                                            Marruecos
                                        </option>
                                        <option value="MZ">
                                            Mozambique
                                        </option>
                                        <option value="MM">
                                            Myanmar
                                        </option>
                                        <option value="NA">
                                            Namibia
                                        </option>
                                        <option value="NR">
                                            Nauru
                                        </option>
                                        <option value="NP">
                                            Nepal
                                        </option>
                                        <option value="NL">
                                            Países Bajos
                                        </option>
                                        <option value="AN">
                                            Países Bajos Antillas
                                        </option>
                                        <option value="NC">
                                            Nueva Caledonia
                                        </option>
                                        <option value="NZ">
                                            Nueva Zelanda
                                        </option>
                                        <option value="NI">
                                            Nicaragua
                                        </option>
                                        <option value="NE">
                                            Níger
                                        </option>
                                        <option value="NG">
                                            Nigeria
                                        </option>
                                        <option value="NU">
                                            Niue
                                        </option>
                                        <option value="NF">
                                            Isla Norfolk
                                        </option>
                                        <option value="MP">
                                            Islas Marianas del Norte
                                        </option>
                                        <option value="NO">
                                            Noruega
                                        </option>
                                        <option value="OM">
                                            Omán
                                        </option>
                                        <option value="PK">
                                            Pakistán
                                        </option>
                                        <option value="PW">
                                            Palau
                                        </option>
                                        <option value="PS">
                                            Territorio Palestino, Ocupado)
                                        </option>
                                        <option value="PA">
                                            Panamá
                                        </option>
                                        <option value="PG">
                                            Papúa Nueva Guinea
                                        </option>
                                        <option value="PY">
                                            Paraguay
                                        </option>
                                        <option value="PE">
                                            Perú
                                        </option>
                                        <option value="PH">
                                            Filipinas
                                        </option>
                                        <option value="PN">
                                            Pitcairn
                                        </option>
                                        <option value="PL">
                                            Polonia
                                        </option>
                                        <option value="PT">
                                            Portugal
                                        </option>
                                        <option value="PR">
                                            Puerto Rico
                                        </option>
                                        <option value="QA">
                                            Katar
                                        </option>
                                        <option value="RE">
                                            Reunión
                                        </option>
                                        <option value="RO">
                                            Rumania
                                        </option>
                                        <option value="RU">
                                            Federación Rusa
                                        </option>
                                        <option value="RW">
                                            Ruanda
                                        </option>
                                        <option value="BL">
                                            Saint Barthélemy
                                        </option>
                                        <option value="SH">
                                            Santa Elena
                                        </option>
                                        <option value="KN">
                                            San Cristóbal y Nieves
                                        </option>
                                        <option value="LC">
                                            Santa Lucía
                                        </option>
                                        <option value="MF">
                                            Saint Martin (Parte francesa)
                                        </option>
                                        <option value="PM">
                                            San Pedro y Miquelón
                                        </option>
                                        <option value="VC">
                                            San Vicente y las Granadinas
                                        </option>
                                        <option value="WS">
                                            Samoa
                                        </option>
                                        <option value="SM">
                                            San Marino
                                        </option>
                                        <option value="ST">
                                            Santo Tomé y Príncipe
                                        </option>
                                        <option value="SA">
                                            Arabia Saudita
                                        </option>
                                        <option value="SN">
                                            Senegal
                                        </option>
                                        <option value="RS">
                                            Serbia
                                        </option>
                                        <option value="SC">
                                            Seychelles
                                        </option>
                                        <option value="SL">
                                            Sierra Leona
                                        </option>
                                        <option value="SG">
                                            Singapur
                                        </option>
                                        <option value="BQ">
                                            Sint Eustatius y Saba Bonaire
                                        </option>
                                        <option value="SX">
                                            Sint Maarten (parte neerlandesa)
                                        </option>
                                        <option value="SK">
                                            Eslovaquia
                                        </option>
                                        <option value="SI">
                                            Eslovenia
                                        </option>
                                        <option value="SB">
                                            Islas Salomón
                                        </option>
                                        <option value="SO">
                                            Somalia
                                        </option>
                                        <option value="ZA">
                                            Sudáfrica
                                        </option>
                                        <option value="GS">
                                            Georgia del Sur y las Islas Sandwich del Sur
                                        </option>
                                        <option value="SS">
                                            Sudán del Sur
                                        </option>
                                        <option value="ES">
                                            España
                                        </option>
                                        <option value="LK">
                                            Sri Lanka
                                        </option>
                                        <option value="SD">
                                            Sudán
                                        </option>
                                        <option value="SR">
                                            Suriname
                                        </option>
                                        <option value="SJ">
                                            Svalbard y Jan Mayen
                                        </option>
                                        <option value="SZ">
                                            Swazilandia
                                        </option>
                                        <option value="SE">
                                            Suecia
                                        </option>
                                        <option value="CH">
                                            Suiza
                                        </option>
                                        <option value="SY">
                                            República Árabe Siria
                                        </option>
                                        <option value="TW">
                                            Taiwán
                                        </option>
                                        <option value="TJ">
                                            Tayikistán
                                        </option>
                                        <option value="TZ">
                                            Tanzania
                                        </option>
                                        <option value="TH">
                                            Tailandia
                                        </option>
                                        <option value="TL">
                                            Timor Oriental
                                        </option>
                                        <option value="TG">
                                            Ir
                                        </option>
                                        <option value="TK">
                                            Tokelau
                                        </option>
                                        <option value="TO">
                                            Tonga
                                        </option>
                                        <option value="TT">
                                            Trinidad y Tobago
                                        </option>
                                        <option value="TN">
                                            Túnez
                                        </option>
                                        <option value="TR">
                                            Turquía
                                        </option>
                                        <option value="TM">
                                            Turkmenistán
                                        </option>
                                        <option value="TC">
                                            Islas Turcas y Caicos
                                        </option>
                                        <option value="TV">
                                            Tuvalu
                                        </option>
                                        <option value="UG">
                                            Uganda
                                        </option>
                                        <option value="UA">
                                            Ucrania
                                        </option>
                                        <option value="AE">
                                            Emiratos Árabes Unidos
                                        </option>
                                        <option value="GB">
                                            Reino Unido
                                        </option>
                                        <option value="US">
                                            Estados Unidos
                                        </option>
                                        <option value="UM">
                                            Islas menores alejadas de los Estados Unidos
                                        </option>
                                        <option value="UY">
                                            Uruguay
                                        </option>
                                        <option value="UZ">
                                            Uzbekistán
                                        </option>
                                        <option value="VU">
                                            Vanuatu
                                        </option>
                                        <option value="VA">
                                            Ciudad del Vaticano
                                        </option>
                                        <option value="VE">
                                            Venezuela
                                        </option>
                                        <option value="VN">
                                            Viet Nam
                                        </option>
                                        <option value="VI">
                                            Islas Vírgenes de EE.UU
                                        </option>
                                        <option value="WF">
                                            Wallis y Futuna
                                        </option>
                                        <option value="EH">
                                            Sahara Occidental
                                        </option>
                                        <option value="YE">
                                            Yemen
                                        </option>
                                        <option value="ZM">
                                            Zambia
                                        </option>
                                        <option value="ZW">
                                            Zimbabue
                                        </option>
                                    </select>
                                    @error('pais')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-6 mb-2">
                                    <input class="form-control @error('email') is-invalid @enderror" type="text" name="email" id="email" placeholder="Email" required value="{{ old('email') }}">
                                    @error('email')
                                    <span class="invalid-feedback text-white" role="alert">
                                        <strong style="font-size:1.2em;">{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-6 mb-2">
                                    <input class="form-control @error('phone') is-invalid @enderror" type="text" name="phone" id="phone" placeholder="{{ trans('welcome.contact.phone') }}" required value="{{ old('phone') }}">
                                    @error('phone')
                                    <span class="invalid-feedback text-white" role="alert">
                                        <strong style="font-size:1.2em;">{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-6 mb-2">
                                    <input class="form-control @error('password') is-invalid @enderror" type="password" name="password" id="password" placeholder="{{ trans('welcome.contact.password') }}" required>
                                    @error('password')
                                    <span class="invalid-feedback text-white" role="alert">
                                        <strong style="font-size:1.2em;">{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-6 mb-2">
                                    <input class="form-control" type="password" name="password_confirmation" id="password-confirm" placeholder="{{ trans('welcome.contact.confirmpassword') }}">
                                </div>
                                <div class="col-12 mb-2">
                                    <p><a href="{{route ('terminos')}}" class="text-white">{{ trans('welcome.contact.terms') }}</a></p>
                                </div>
                                <div class="col-12">
                                    <button onclick="gtag('event', 'send', {'event_Lead': 'info','event_Form': 'click'});" type="submit" class="btn btn-block btn btn-danger mb-2">{{ trans('buttons.register') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        
        <section id="faq">
            <!-- Pregunta Frecuentes -->
            <h3 class="text-center m-5">{{ trans('welcome.faq.title') }}</h3>
            <div class="accordion col-md-10 pb-5" style="margin: 0 auto;" id="accordionExample">
                <div class="card">
                    <div class="card-header" style="background-color: #f15f79;" id="headingOne">
                        <h2 class="mb-0">
                            <button class="btn btn-link text-white" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                {{ trans('welcome.faq.question1') }}
                            </button>
                        </h2>
                    </div>
                    
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                            {{ trans('welcome.faq.answer1') }}
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" style="background-color: #f15f79;" id="headingTwo">
                        <h2 class="mb-0">
                            <button class="btn btn-link text-white collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                {{ trans('welcome.faq.question2') }}
                            </button>
                        </h2>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                            {{ trans('welcome.faq.answer2') }}
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" style="background-color: #f15f79;" id="headingThree">
                        <h2 class="mb-0">
                            <button class="btn btn-link text-white collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                {{ trans('welcome.faq.question3') }}
                            </button>
                        </h2>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div class="card-body">
                            {{ trans('welcome.faq.answer3') }}
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" style="background-color: #f15f79;" id="headingFour">
                        <h2 class="mb-0">
                            <button class="btn btn-link text-white" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                {{ trans('welcome.faq.question4') }}
                            </button>
                        </h2>
                    </div>
                    
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                        <div class="card-body">
                            {{ trans('welcome.faq.answer4') }}
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" style="background-color: #f15f79;" id="headingFive">
                        <h2 class="mb-0">
                            <button class="btn btn-link text-white" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                {{ trans('welcome.faq.question5') }}
                            </button>
                        </h2>
                    </div>
                    
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                        <div class="card-body">
                            {{ trans('welcome.faq.answer5') }}
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" style="background-color: #f15f79;" id="headingSix">
                        <h2 class="mb-0">
                            <button class="btn btn-link text-white" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                {{ trans('welcome.faq.question6') }}
                            </button>
                        </h2>
                    </div>
                    
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
                        <div class="card-body">
                            {{ trans('welcome.faq.answer6') }}
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        
        <!-- Footer -->
        <footer class="footer bg-light">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 h-100 text-center text-lg-left my-auto">
                        <ul class="list-inline mb-2">
                            <li class="list-inline-item">
                                <a href="#">{{ trans('welcome.footer.home') }}</a>
                            </li>
                            <li class="list-inline-item">&sdot;</li>
                            <li class="list-inline-item">
                                <a href="{{route ('login')}}">{{ trans('welcome.footer.login') }}</a>
                            </li>
                            <li class="list-inline-item">&sdot;</li>
                            <li class="list-inline-item">
                                <a href="#showcase">{{ trans('welcome.footer.moreinfo') }}</a>
                            </li>
                            <li class="list-inline-item">&sdot;</li>
                            <li class="list-inline-item">
                                <a href="#faq">{{ trans('welcome.footer.faq') }}</a>
                            </li>
                        </ul>
                        <p class="text-muted small mb-4 mb-lg-0">&copy; {{ trans('welcome.footer.copyright') }}</p>
                    </div>
                </div>
            </footer>
            
            <!-- Contact Java Script -->
            <script src="js/contact.js"></script>
            
            <!-- Bootstrap core JavaScript -->
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
            
            <!-- Smoth Scrolling -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
            <script>
                $(document).ready(function(){
                    // Add smooth scrolling to all links
                    $("a").on('click', function(event) {
                        
                        // Make sure this.hash has a value before overriding default behavior
                        if (this.hash !== "") {
                            // Prevent default anchor click behavior
                            event.preventDefault();
                            
                            // Store hash
                            var hash = this.hash;
                            
                            // Using jQuery's animate() method to add smooth page scroll
                            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                            $('html, body').animate({
                                scrollTop: $(hash).offset().top
                            }, 800, function(){
                                
                                // Add hash (#) to URL when done scrolling (default click behavior)
                                window.location.hash = hash;
                            });
                        } // End if
                    });
                });
            </script>
            
            <!-- Start of HubSpot Embed Code -->
            <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/6012790.js"></script>
            <!-- End of HubSpot Embed Code -->
            
            <!-- Start of HubSpot Embed Code -->
            <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/6012790.js"></script>
            <!-- End of HubSpot Embed Code -->
        </body>
        
        </html>
        