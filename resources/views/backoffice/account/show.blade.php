@extends('layouts.app')

@section('content')

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Account - {{ $account->username }}</h1>
</div>

<div class="row">
    <div class="col-lg-4">
        <!-- Basic Card Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Hastags # de Referencia</h6>
            </div>
            <div class="card-body">
                <form action="{{ route('backoffice.account.hashtags.store', $account->id) }}" method="POST">
                    @csrf
                    @php $hashtag_count = 1; @endphp
                    @foreach ($account->hashtags as $key => $hashtag)
                    <div class="form-group">
                        <label for="hashtag-{{ $hashtag_count }}">Hashtag {{ $hashtag_count }}</label>
                        <input type="text" name="hashtags[]" id="hashtag-{{$hashtag_count}}" class="form-control" value="{{$hashtag->value}}">
                    </div>
                    @php $hashtag_count++; @endphp
                    @endforeach
                    <div class="form-group">
                        <label for="hashtag-{{ $hashtag_count }}">Hashtag {{ $hashtag_count }}</label>
                        <input type="text" name="hashtags[]" id="hashtag-{{$hashtag_count}}" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <!-- Basic Card Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Cuentas de Referencia</h6>
            </div>
            <div class="card-body">
                <p>Los nombres de usuario de tus competidores. Usamos sus cuentas para interactuar con sus seguidores, la gente que les comenta y hace likes.</p>
                <form action="{{ route('backoffice.account.store') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Referencia 1:</label>
                        <input type="email" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <!-- Basic Card Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Funcionalidades</h6>
            </div>
            <div class="card-body">
                <p>Seleccione las funcionalidades que quieres que usemos en tu cuenta.</p>
                <form action="{{ route('backoffice.account.store') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label for="exampleInputEmail1">Seguir y dejar de Seguir:</label>
                            <input type="email" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="exampleInputEmail1">Ignorar Usuarios Privados:</label>
                            <input type="email" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="exampleInputEmail1">Likes:</label>
                            <input type="email" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="exampleInputEmail1">Comentarios:</label>
                            <input type="email" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
    
</div>

@endsection
