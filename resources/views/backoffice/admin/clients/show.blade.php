@extends('layouts.app')
@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Usuario: {{$user->name}} | Email: {{$user->email}} || Fecha: {{$user->created_at->diffForHumans()}} || País: {{$user->pais}}</h6>
    </div>
    <div class="row">
        <div class="col ml-3">
            <p>Nombre de Usuario: {{ $user->cuenta }}</p>
            <p>Contraseña: {{ $user->igpassword }}</p>
            <div class="row">
                <div class="col">
                    <p>Referencias:</p>
                    <ul>
                        <li>{{ $user->referencia1 }}</li>
                        <li>{{ $user->referencia2 }}</li>
                        <li>{{ $user->referencia3 }}</li>
                        <li>{{ $user->referencia4 }}</li>
                        <li>{{ $user->referencia4 }}</li>
                    </ul>
                </div>
                <div class="col">
                    <p>Hashtags:</p>
                    <ul>
                        <li>{{ $user->hashtag1 }}</li>
                        <li>{{ $user->hashtag2 }}</li>
                        <li>{{ $user->hashtag3 }}</li>
                        <li>{{ $user->hashtag4 }}</li>
                        <li>{{ $user->hashtag4 }}</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col">
            {{-- <a href="{{route ('alta', $user->id)}}">Alta Usuario</a> --}}
            @if($user->cliente)
                <div>
                    <a href="{{route ('baja', $user->id)}}">Baja Cliente</a>
                </div>
            @endif
            <p>Estado: {{ $user->showEstado() }} || Assigned To: {{ $user->showAssignedTo() }}</p>
            <p>Ubicaciónes: {{ $user->customizations()[0]->ubicaciones }}</p>
            <p>Comentarios:</p>
            <div class="row">
                <div class="col">
                    <ul>
                        <li>{{ $user->customizations()[0]->comment1 }}</li>
                        <li>{{ $user->customizations()[0]->comment2 }}</li>
                        <li>{{ $user->customizations()[0]->comment3 }}</li>
                        <li>{{ $user->customizations()[0]->comment4 }}</li>
                        <li>{{ $user->customizations()[0]->comment5 }}</li>
                    </ul>
                </div>
                <div class="col">
                    <ul>
                        <li>{{ $user->customizations()[0]->comment6 }}</li>
                        <li>{{ $user->customizations()[0]->comment7 }}</li>
                        <li>{{ $user->customizations()[0]->comment8 }}</li>
                        <li>{{ $user->customizations()[0]->comment9 }}</li>
                        <li>{{ $user->customizations()[0]->comment10 }}</li>
                    </ul>
                </div>
            </div>
            <p>Mensaje Directo: {{ $user->customizations()[0]->md }}</p>
            <p>Quiere Likes? {{ $user->customizations()[0]->likes }}</p>
            <p>Skip Private? {{ $user->customizations()[0]->private }}</p>
            <p>Comentarios? {{ $user->customizations()[0]->comment }}</p>
            <p>Seguir y Dejar de Seguir? {{ $user->customizations()[0]->follow }}</p>
        </div>
    </div>
<div>

@endsection
