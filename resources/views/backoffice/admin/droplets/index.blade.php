@extends('layouts.app')

@section('content')

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-server mx-3"></i>Droplets</h1>
</div>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Droplets list</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>IP</th>
                        <th>Cuentas activas</th>
                        <th>Fecha de creacion</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($droplets as $droplet)
                    <tr>
                        <th scope="row">{{$droplet->id}}</th>
                        <td>{{$droplet->name}}</td>
                        <td>{{$droplet->ip}}</td>
                        <td>{{$droplet->accounts->count()}}</td>
                        <td>{{$droplet->created_at->format('d-m-Y H:i:s')}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
