<?php

return [
    'sidebar' => [
        'clients' => 'Clients',
        'accounts' => 'Accounts',
        'droplets' => 'Droplets',
        'admin' => 'Admin',
    ],
];