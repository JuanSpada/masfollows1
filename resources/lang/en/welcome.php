<?php

return [
    'nav' => [
        'login' => 'Login',
        'register' => 'Register',
    ],
    'slider' => [
        'title' => 'Grow your Followers on Instagram',
        'subtitle' => 'Growth software for Instagram, real growth, real followers'
    ],
    'section1'=>[
        'title1' => 'Identify your Competitors',
        'title2' => 'Ideal Audience',
        'title3' => 'Trust',
        'desc1' => 'Search your competitors accounts or the accounts that serve as a reference for your Instagram account.',
        'desc2' => "You'll reach your ideal audience, get more followers, likes, comments and people who visit your account!",
        'desc3' => 'We already work for a large number of brands, influencers and artists. What are you waiting for?',
    ],
    'section2'=>[
        'title1' => 'Quality Followers',
        'title2' => 'How it Works?',
        'title3' => 'What does it do?',
        'desc1' => 'Grow your instagram followers organically in a segmented way and get real followers thanks to our technology supported by artificial intelligence. Through our technology we grow your natural presence among your target audience, achieving sustainable and quality growth!',
        'desc2' => "Can't you dedicate 4/5 hours a day to Instagram? More Follows is the solution to this problem. Our technology based on artificial intelligence creates natural interactions within Instagram (Follows / Likes / Views) so that you can grow your account as you would, dedicating it 4/5 hours a day. No robots, no fake accounts. Everything natural.",
        'desc3' => "We use # and referring users to reach the people of your target audience, specific users and their followers, interacting in the best way. We optimize the whole process so that every day that passes the results are better, you are not alone, the accounts with more followers in the world use these technologies.",
    ],
    'contact' => [
        'title' => '¡Register Now!',
        'name' => 'Name',
        'country' => 'Where are you from?',
        'phone' => 'Phone Number',
        'password' => 'Password',
        'confirmpassword' => 'Confirm your password',
        'terms' => 'By registering I accept the terms and conditions of Masfollows.',
    ],
    'faq' => [
        'title' => 'Frequent Questions',
        'question1' => 'The results are guaranteed?',
        'answer1' => 'We cannot guarantee the results since it depends on many factors external to us (Instagram limits, content quality etc.). Until today we have not encountered any profile that we are not able to grow. The monthly average exceeds 2,000 monthly followers but if you have good content and your segment is interesting you can grow above 4,000 followers a month easily.',
        'question2' => 'Are the followers real?',
        'answer2' => "Totally ! We are not a company selling followers or likes. Our technology is what interacts and energizes our clients' accounts with real profiles, which generates organic growth. Nobody is going to keep you obligated but they will do it because the content you have they like.",
        'question3' => 'How do you identify my audience?',
        'answer3' => "We usually ask you to identify 10 accounts / hashtags that correspond to your target audience. Based on this, our team develops lists of related accounts and hashtags with which to start interacting. Our artificial intelligence system based on these results is identifying which accounts are more optimal for your profile based on the number of posts, the last time you published and 8 other variables to optimize the account and ensure that the followers we get are assets and generate engagment.",
        'question4' => 'If I cancel my plan. Will my followers disappear?',
        'answer4' => "Of course not . The followers you have gained with MásFollows are yours. Remember that the followers you have gained have done so by actively choosing you, so they are showing interest in your profile and your content. Of course they are also free to stop following you if they are not interested so we recommend that you continue to generate content as before.",
        'question5' => 'Do I need to give my account password?',
        'answer5' => 'Yes, we need access to your account so that our technology can work with it. We need to interact with your audience using your account. Of course we will not exchange your password with anyone and it will be all in a secure environment.',
        'question6' => 'Can I use my account simultaneously?',
        'answer6' => "Yes, there is no problem in using your account normally. Once hired we will make some recommendations regarding limits in order not to exceed Instagram limits.",
    ],
    'footer' => [
        'home' => 'Home',
        'login' => 'Login',
        'moreinfo' => 'More Info',
        'faq' => 'Frequent Questions',
        'copyright' => 'MásFollows 2019. All rights reserved.',
    ]
];