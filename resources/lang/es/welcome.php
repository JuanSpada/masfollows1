<?php

return [
    'nav' => [
        'login' => 'Iniciar Sesión',
        'register' => 'Registrarse',
    ],
    'slider' => [
        'title' => 'Aumentar Seguidores en Instagram',
        'subtitle' => 'Sistema de crecimiento para Instagram, crecimiento real, seguidores reales'
    ],
    'section1' =>[
        'title1' => 'Identifica tus Competidores',
        'title2' => 'Público Ideal',
        'title3' => 'Confianza',
        'desc1' => 'Busca las cuentas de tus competidores o las cuentas que sirvan de referencia para tu cuenta de Instagram.',
        'desc2' => 'Vas a llegar a tu público ideal, obtener más seguidores, likes, comentarios y gente que visita tu cuenta!',
        'desc3' => 'Ya trabajamos para una gran cantidad de marcas, influencers y artistas. ¿Q qué esperas?',
    ],
    'section2' =>[
        'title1' => 'Seguidores de Calidad',
        'title2' => '¿Cómo Funciona?',
        'title3' => 'Qué Hace?',
        'desc1' => 'Haz crecer tus seguidores de instagram orgánicamente de forma segmentada y obten seguidores reales gracias a nuestra tecnología apoyada en inteligencia artificial. Mediante nuestra tecnología hacemos crecer tu presencia natural entre tu público objetivo consiguiendo un crecimiento sostenible y de calidad!',
        'desc2' => 'Nuestra tecnología esta basada en inteligencia artificial que crea interacciones "naturales" dentro de Instagram ( Follows / Likes / Views ) de forma que consigue hacer crecer tu cuenta de manera exponencial. Sin robots , sin cuentas falsas. Todo natural.',
        'desc3' => 'Usamos # y usuarios referentes para llegar a las personas de tu público objetivo, usuarios específicos y sus seguidores, interactuando de la mejor manera. Optimizamos todo el proceso para que cada día que pasa los resultados sean mejores, no estas solo, las cuentas con más seguidores del mundo utilizan estas tecnologías.',
    ],
    'contact' => [
        'title' => '¡Registrate Ahora!',
        'name' => 'Nombre',
        'country' => 'De donde sos?',
        'phone' => 'Teléfono',
        'password' => 'Contraseña',
        'confirmpassword' => 'Confirma tu Contraseña',
        'terms' => 'Al Registrarme Acepto los términos y condiciones de MásFollows',
    ],
    'faq' => [
        'title' => 'Preguntas Frecuentes',
        'question1' => '¿Los datos son garantizados?',
        'answer1' => 'No podemos garantizar los resultados ya que depende de muchos factores externos a nosotros ( límites de Instagram, calidad de contenido etc ) .Hasta hoy no nos hemos encontrado con ningún perfil que no seamos capaz de hacer crecer.',
        'question2' => '¿Los seguidores son reales?',
        'answer2' => 'Totalmente ! No somos una empresa de venta de seguidores o likes . Nuestra tecnología lo que hace es interactuar y dinamizar las cuentas de nuestros clientes con perfiles reales , lo que genera un crecimiento orgánico. Nadie te va a seguir obligado sino que lo hará porque el contenido que tienes les gusta.',
        'question3' => '¿Cómo identifican mi audiencia?',
        'answer3' => 'Solemos pedirles que identifiquéis 10 cuentas / hashtags que correspondan con tu audiencia objetivo. En base a esto nuestro equipo elabora unos listados de cuentas y hashtags afines con las que empezar a interactuar. Nuestro sistema de inteligencia artificial basándose en estos resultados va identificando que cuentas son más optimas para tu perfil en base al número de posts, la última que vez que publicó y otras 8 variables para ir optimizando la cuenta y asegurarse de que los seguidores que conseguimos son activos y generan engagment.',
        'question4' => 'Si cancelo mi plan. ¿Desaparecen mis seguidores?',
        'answer4' => 'Por supuesto que no . Los seguidores que has ganado con MásFollows son tuyos. Recuerda que los seguidores que has ganado lo han hecho eligiéndote activamente , por lo que están demostrando interés en tu perfil y tu contenido. Son también libres de dejar de seguirte si no les interesa así que os recomendamos que sigáis generando contenido como hasta ese momento.',
        'question5' => '¿Necesito darles la contraseña de mi cuenta?',
        'answer5' => 'Si, necesitamos el acceso a tu cuenta para que nuestra tecnología pueda trabajar con ella . Necesitamos interactuar con tu audiencia usando tu cuenta . Por supuesto no intercambiaremos con nadie tu contraseña y estará todo en un entorno seguro.',
        'question6' => '¿Puedo usar mi cuenta simuntaneamente?',
        'answer6' => 'Si , no hay problema en que uses tu cuenta normalmente . Una vez contratado te haremos algunas recomendaciones en cuanto a límites de cara a no sobrepasar los límites de Instagram.',
    ],
    'footer' => [
        'home' => 'Inicio',
        'login' => 'Iniciar Sesión',
        'moreinfo' => 'Más Información',
        'faq' => 'Preguntas Frecuentes',
        'copyright' => 'MásFollows 2019. Todos los Derechos Reservados.',
    ]
];