<?php

return [
    'sidebar' => [
        'accounts' => 'Cuentas',
        'admin' => 'Admin',
        'droplets' => 'Droplets',
        'clients' => 'Clients',
    ],
];