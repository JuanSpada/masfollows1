<?php

use App\User;
use App\Account;
use App\Hashtag;
use App\Reference;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateToNewStructure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $users = User::all();
        foreach ($users as $user) {
            if ($user->cuenta) {
                $account = new Account();
                $account->username = $user->cuenta;
                $account->password = '';
                if ($user->igpassword) {
                    $account->password = $user->igpassword;
                }
                $account->user_id = $user->id;
                $account->status = $user->estado;
                $account->created_at = $user->created_at;
                $account->updated_at = $user->updated_at;
                $account->save();

                if ($user->hashtag1 != null) {
                    $hashtag = new Hashtag();
                    $hashtag->account_id = $account->id;
                    $hashtag->value = $user->hashtag1;
                    $hashtag->save();
                }

                if ($user->hashtag2 != null) {
                    $hashtag = new Hashtag();
                    $hashtag->account_id = $account->id;
                    $hashtag->value = $user->hashtag2;
                    $hashtag->save();
                }

                if ($user->hashtag4 != null) {
                    $hashtag = new Hashtag();
                    $hashtag->account_id = $account->id;
                    $hashtag->value = $user->hashtag4;
                    $hashtag->save();
                }

                if ($user->hashtag4 != null) {
                    $hashtag = new Hashtag();
                    $hashtag->account_id = $account->id;
                    $hashtag->value = $user->hashtag4;
                    $hashtag->save();
                }

                if ($user->referencia1 != null) {
                    $reference = new Reference();
                    $reference->account_id = $account->id;
                    $reference->value = $user->referencia1;
                    $reference->save();
                }

                if ($user->referencia2 != null) {
                    $reference = new Reference();
                    $reference->account_id = $account->id;
                    $reference->value = $user->referencia2;
                    $reference->save();
                }

                if ($user->referencia3 != null) {
                    $reference = new Reference();
                    $reference->account_id = $account->id;
                    $reference->value = $user->referencia3;
                    $reference->save();
                }

                if ($user->referencia4 != null) {
                    $reference = new Reference();
                    $reference->account_id = $account->id;
                    $reference->value = $user->referencia4;
                    $reference->save();
                }
            }
        }
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('cuenta');
            $table->dropColumn('igpassword');
            $table->dropColumn('hashtag1');
            $table->dropColumn('hashtag2');
            $table->dropColumn('hashtag3');
            $table->dropColumn('hashtag4');
            $table->dropColumn('referencia1');
            $table->dropColumn('referencia2');
            $table->dropColumn('referencia3');
            $table->dropColumn('referencia4');
            $table->dropColumn('script');
            $table->dropColumn('estado');
            $table->dropColumn('assigned_to');
            $table->integer('role')->after('avatar')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('new_structure', function (Blueprint $table) {
            //
        });
    }
}
