<?php

return [
    'enabled' => env('ZAPIER_ENABLED'),
    'url' => env('ZAPIER_URL'),

];
