<?php

return [
    'enabled' => env('DIGITAL_OCEAN_ENABLED'),
    'token' => env('DIGITAL_OCEAN_TOKEN'),
    'url' => env('DIGITAL_OCEAN_URL'),
];