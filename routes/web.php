<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/migrate', 'LinkController@cache');
// Route::get('/key', 'LinkController@artisankey');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('auth/{provider}', 'Auth\SocialAuthController@redirectToProvider')->name('social.auth');
Route::get('auth/{provider}/callback', 'Auth\SocialAuthController@handleProviderCallback');
Route::post('/home', 'HomeController@mercadopago')->name('mercadopago');
Route::get('/terminos', 'LinkController@terminos')->name('terminos');


Route::middleware(['auth'])->name('backoffice.')->group(function() {

    Route::prefix('/account')->name('account.')->group(function() {
        Route::get('/', 'AccountController@index')->name('index');
        Route::get('/create', 'AccountController@create')->name('create');
        Route::post('/store', 'AccountController@store')->name('store');
        Route::get('/{id}', 'AccountController@show')->name('show');
        Route::prefix('/{account_id}/hashtags')->name('hashtags.')->group(function() {
            Route::post('/store', 'HashtagsController@store')->name('store');
        });
    });

    Route::prefix('/profile')->name('profile.')->group(function() {
        Route::get('/', 'ProfileController@index')->name('index');
    });

    Route::prefix('setttings')->name('settings.')->group(function() {
        Route::get('/', 'LinkController@configurar')->name('index');
    });

    Route::middleware(['admin'])->prefix('/admin')->name('admin.')->group(function () {

        Route::prefix('/clients')->name('clients.')->group(function() {
            Route::get('/', 'ClientsController@index')->name('index');
            Route::get('/{id}', 'ClientsController@show')->name('show');
        });

        Route::prefix('/droplets')->name('droplets.')->group(function() {
            Route::get('/', 'DropletsController@index')->name('index');
            Route::post('/store', 'LinkController@store')->name('store');
            Route::delete('/destroy', 'DropletsController@destroy')->name('destroy');
        });

    });


    Route::get('/configurar', 'LinkController@configurar')->name('configurar');
    Route::post('/configurar', 'HomeController@update')->name('update');
    Route::get('{id}/baja', 'HomeController@baja')->name('baja');
    Route::get('/pagoconfirmado', 'LinkController@pagoconfirmado')->name('pagoconfirmado');
});


//RUTAS BACKOFFICE
Route::group(['middleware' => ['auth', 'admin']], function () {
    Route::get('/gestion', 'LinkController@backoffice')->name('backoffice');
    Route::get('/gestion/{id}', 'LinkController@showUser')->name('showUser');
    Route::get('/migrate', 'LinkController@migrate');
});
